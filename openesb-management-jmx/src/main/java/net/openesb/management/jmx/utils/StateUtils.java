package net.openesb.management.jmx.utils;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;
import javax.jbi.management.DeploymentServiceMBean;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StateUtils {
    
    /**
     * any framework state
     */
    protected static final int    ANY_FRAMEWORK_COMPONENT_STATE = -1;
    
    /**
     * sa started state
     */
    protected static final String FRAMEWORK_SA_STARTED_STATE    = DeploymentServiceMBean.STARTED;
    
    /**
     * sa stopped state
     */
    protected static final String FRAMEWORK_SA_STOPPED_STATE    = DeploymentServiceMBean.STOPPED;
    
    /**
     * sa shutdown state
     */
    protected static final String FRAMEWORK_SA_SHUTDOWN_STATE   = DeploymentServiceMBean.SHUTDOWN;
    
    /**
     * any state
     */
    public static final String FRAMEWORK_SA_ANY_STATE        = "any";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_STARTED_STATE    = "started";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_STOPPED_STATE    = "stopped";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_SHUTDOWN_STATE   = "shutdown";
    
    /**
     * state
     */
    public static final String FRAMEWORK_SU_UNKNOWN_STATE    = "unknown";
    
    public static final String          RUNNING_STATE                       = "Running";
    
    /**
     * converts to deployment service service assembly state.
     * 
     * @return state
     * @param uiState
     *            state
     */
    public static String toFrameworkServiceAssemblyState(String uiState) {
        if (uiState == null) {
            return FRAMEWORK_SA_ANY_STATE;
        }
        
        String saState = uiState.trim();
        
        if (saState.length() <= 0) {
            return FRAMEWORK_SA_ANY_STATE;
        }
        
        if (ServiceAssemblyInfo.STARTED_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_STARTED_STATE;
        } else if (ServiceAssemblyInfo.STOPPED_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_STOPPED_STATE;
        } else if (ServiceAssemblyInfo.SHUTDOWN_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_SHUTDOWN_STATE;
        } else { // any other value
            return FRAMEWORK_SA_ANY_STATE;
        }
    }
    
    /**
     * converts to ui service unit state
     * 
     * @return state
     * @param frameworkState
     *            state
     */
    public static String toUiServiceUnitState(String frameworkState) {
        String uiState = ServiceUnitInfo.UNKNOWN_STATE;
        
        if (frameworkState == null) {
            return ServiceUnitInfo.UNKNOWN_STATE;
        }
        
        String suState = frameworkState.trim();
        
        if (suState.length() <= 0) {
            return ServiceUnitInfo.UNKNOWN_STATE;
        }
        
        if (FRAMEWORK_SU_STARTED_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.STARTED_STATE;
        } else if (FRAMEWORK_SU_STOPPED_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.STOPPED_STATE;
        } else if (FRAMEWORK_SU_SHUTDOWN_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.SHUTDOWN_STATE;
        } else {
            uiState = ServiceUnitInfo.UNKNOWN_STATE;
        }
        return uiState;
    }
    
    /**
     * converts to ui service assembly state
     * 
     * @return state
     * @param frameworkState
     *            state
     */
    public static String toUiServiceAssemblyState(String frameworkState) {
        String uiState = ServiceAssemblyInfo.UNKNOWN_STATE;
        
        if (frameworkState == null) {
            return ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        
        String saState = frameworkState.trim();
        
        if (saState.length() <= 0) {
            return ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        
        if (FRAMEWORK_SA_STARTED_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.STARTED_STATE;
        } else if (FRAMEWORK_SA_STOPPED_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.STOPPED_STATE;
        } else if (FRAMEWORK_SA_SHUTDOWN_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.SHUTDOWN_STATE;
        } else {
            uiState = ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        return uiState;
    }
    
    /**
     * framework states
     * 
     * @return state
     * @param uiCompState
     *            state
     */
    public static ComponentState toFrameworkComponentInfoState(
            String uiCompState) {
        if (uiCompState == null) {
            return ComponentState.UNKNOWN;
        }
        
        String compState = uiCompState.trim();
        
        if (compState.length() <= 0) {
            return ComponentState.UNKNOWN;
        }
        
        if (JBIComponentInfo.SHUTDOWN_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.SHUTDOWN;
        } else if (JBIComponentInfo.STARTED_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.STARTED;
        } else if (JBIComponentInfo.STOPPED_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.STOPPED;
        } else { // any other value
            return ComponentState.UNKNOWN;
        }
    }
    
    /**
     * converts type
     * 
     * @return type
     * @param frameworkCompType
     *            type
     */
    public static String toUiComponentInfoType(
            ComponentType frameworkCompType) {
        String uiType = JBIComponentInfo.UNKNOWN_TYPE;
        switch (frameworkCompType) {
            case BINDING:
                uiType = JBIComponentInfo.BINDING_TYPE;
                break;
            case ENGINE:
                uiType = JBIComponentInfo.ENGINE_TYPE;
                break;
            case SHARED_LIBRARY:
                uiType = JBIComponentInfo.SHARED_LIBRARY_TYPE;
                break;
            default:
                uiType = JBIComponentInfo.UNKNOWN_TYPE;
                break;
        }
        return uiType;
    }
    
    /**
     * converts state
     * 
     * @return state
     * @param frameworkCompState
     *            state
     */
    public static String toUiComponentInfoState(
            ComponentState frameworkCompState) {
        String uiState = JBIComponentInfo.UNKNOWN_STATE;
        switch (frameworkCompState) {
            case SHUTDOWN:
                uiState = JBIComponentInfo.SHUTDOWN_STATE;
                break;
            case STARTED:
                uiState = JBIComponentInfo.STARTED_STATE;
                break;
            case STOPPED:
                uiState = JBIComponentInfo.STOPPED_STATE;
                break;
            default:
                uiState = JBIComponentInfo.UNKNOWN_STATE;
                break;
        }
        return uiState;
    }
}
