package net.openesb.management.jmx;

import java.util.HashSet;
import java.util.Set;
import net.openesb.model.api.Endpoint;
import net.openesb.management.api.EndpointService;
import net.openesb.management.api.ManagementException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class EndpointServiceImpl extends AbstractServiceImpl implements EndpointService {

    @Override
    public Set<Endpoint> findEndpoints(String componentName, boolean consume) throws ManagementException {
        String[] aEndpoints;
        
        if (componentName == null) {
            if (consume) {
                aEndpoints = getMessageServiceMBean().getActiveConsumingEndpoints();
            } else {
                aEndpoints = getMessageServiceMBean().getActiveEndpoints();
            }
        } else {
            if (consume) {
                aEndpoints = getMessageServiceMBean().getActiveConsumingEndpoints(componentName);
            } else {
                aEndpoints = getMessageServiceMBean().getActiveEndpoints(componentName);
            }
        }

        Set<Endpoint> endpoints = new HashSet<Endpoint>(aEndpoints.length);
        for (String sEndpoint : aEndpoints) {
            endpoints.add(new Endpoint(sEndpoint, consume));
        }

        return endpoints;
    }
}
