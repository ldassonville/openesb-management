package net.openesb.management.api;

import java.util.Set;
import net.openesb.model.api.Endpoint;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface EndpointService {
    
    Set<Endpoint> findEndpoints(String componentName, boolean consume) throws ManagementException;
}
