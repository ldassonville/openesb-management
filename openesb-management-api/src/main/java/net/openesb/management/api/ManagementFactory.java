package net.openesb.management.api;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public final class ManagementFactory {
    
    private static ServiceAssemblyService serviceAssemblyService;
    private static SharedLibraryService sharedLibraryService;
    private static ComponentService componentService;
    private static EndpointService endpointService;
    private static AdministrationService administrationService;
    private static MessageService messageService;
    private static StatisticsService statisticsService;
    private static ConfigurationService configurationService;
    private static JvmService jvmService;
    
    private ManagementFactory() {
        // Private constructor to avoid instance.
    }
    
    public synchronized  static ServiceAssemblyService getServiceAssemblyService() {
        if (serviceAssemblyService == null) {
            serviceAssemblyService = 
                    constructInstance("net.openesb.management.jmx.ServiceAssemblyServiceImpl");
        }
        
        return serviceAssemblyService;
    }
    
    public synchronized static SharedLibraryService getSharedLibraryService() {
        if (sharedLibraryService == null) {
            sharedLibraryService = 
                    constructInstance("net.openesb.management.jmx.SharedLibraryServiceImpl");
        }
        
        return sharedLibraryService;
    }
    
    public synchronized static ComponentService getComponentService() {
        if (componentService == null) {
            componentService = 
                    constructInstance("net.openesb.management.jmx.ComponentServiceImpl");
        }
        
        return componentService;
    }
    
    public synchronized static EndpointService getEndpointService() {
        if (endpointService == null) {
            endpointService = 
                    constructInstance("net.openesb.management.jmx.EndpointServiceImpl");
        }
        
        return endpointService;
    }
    
    public synchronized static AdministrationService getAdministrationService() {
        if (administrationService == null) {
            administrationService = 
                    constructInstance("net.openesb.management.jmx.AdministrationServiceImpl");
        }
        
        return administrationService;
    }
    
    public synchronized static MessageService getMessageService() {
        if (messageService == null) {
            messageService = 
                    constructInstance("net.openesb.management.jmx.MessageServiceImpl");
        }
        
        return messageService;
    }
    
    public synchronized static StatisticsService getStatisticsService() {
        if (statisticsService == null) {
            statisticsService = 
                    constructInstance("net.openesb.management.jmx.StatisticsServiceImpl");
        }
        
        return statisticsService;
    }
    
    public synchronized static ConfigurationService getConfigurationService() {
        if (configurationService == null) {
            configurationService = 
                    constructInstance("net.openesb.management.jmx.ConfigurationServiceImpl");
        }
        
        return configurationService;
    }
    
    public synchronized static JvmService getJvmService() {
        if (jvmService == null) {
            jvmService = 
                    constructInstance("net.openesb.management.jmx.JvmServiceImpl");
        }
        
        return jvmService;
    }
 
    private static <T> T constructInstance(String serviceClassname) {
        try {
            return (T) Class.forName(serviceClassname).newInstance();
        } catch (Exception e) {
            e.printStackTrace();;
            return null;
        }
    }
    
}
