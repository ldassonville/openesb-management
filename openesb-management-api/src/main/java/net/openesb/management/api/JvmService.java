package net.openesb.management.api;

import java.util.Map;
import net.openesb.model.api.metric.Metric;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface JvmService {

    Map<String, Object> getInformations();
    
    Map<String, Metric> getMemoryUsage();
    
    Map<String, Metric> getGarbageCollector();
    
    Map<String, Metric> getThreadStates();
}
