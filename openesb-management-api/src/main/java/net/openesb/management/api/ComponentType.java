package net.openesb.management.api;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public enum ComponentType {

    BINDING,
    ENGINE,
    SHARED_LIBRARY,
    BINDINGS_AND_ENGINES,
    ALL
};
