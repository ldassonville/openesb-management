package net.openesb.management.api;

import java.util.Map;
import net.openesb.model.api.metric.Metric;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface StatisticsService {

    Map<String, Metric> getEndpointStatistics(String endpointName) throws ManagementException;
    
    Map<String, Metric> getComponentStatistics(String componentName) throws ManagementException;
    
    Map<String, Metric> getServiceAssemblyStatistics(String serviceAssemblyName) throws ManagementException;
}
