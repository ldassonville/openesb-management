package net.openesb.management.api;

import java.util.Set;
import java.util.logging.Level;
import net.openesb.model.api.ComponentDescriptor;
import net.openesb.model.api.JBIComponent;
import net.openesb.model.api.Logger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface ComponentService {
    
    /**
     * return component info which satisfies the options passed to the method.
     *
     * @param state return all the binding components that are in the specified
     * state. valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED or
     * null for ANY state
     * @param sharedLibraryName return all the binding components that have a
     * dependency on the specified shared library. null value to ignore this
     * option.
     * @param serviceAssemblyName return all the binding components that have
     * the specified service assembly deployed on them. null value to ignore
     * this option.
     * @return List of binding component infos
     * @throws ManagementException if error or exception occurs.
     */
    Set<JBIComponent> findComponents(ComponentType type, String state,
            String sharedLibraryName, String serviceAssemblyName) throws ManagementException;
    
    JBIComponent getComponent(String componentName) throws ManagementException;
    
    ComponentDescriptor getDescriptor(String componentName) throws ManagementException;
    
    String getDescriptorAsXml(String componentName) throws ManagementException;
    
    void start(String componentName) throws ManagementException;
    
    void stop(String componentName) throws ManagementException;
    
    void shutdown(String componentName, boolean force) throws ManagementException;
    
    String install(String cmpZipURL) throws ManagementException;
    
    void uninstall(String componentName, boolean force) throws ManagementException;
    
    void upgrade(String componentName, String cmpZipURL) throws ManagementException;
    
    Set<Logger> getLoggers(String componentName) throws ManagementException;
    
    void setLoggerLevel(String componentName, String loggerName, Level loggerLevel) throws ManagementException;
}
