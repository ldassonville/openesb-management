package net.openesb.management.api;

import java.util.Set;
import net.openesb.model.api.ComponentDescriptor;
import net.openesb.model.api.SharedLibrary;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface SharedLibraryService {
    
    /**
     * returns the list of Shared Library infos.
     *
     * @param componentName to return only the shared libraries that are this
     * component dependents. null for listing all the shared libraries in the
     * system.
     * @return List of componentinfos for shared libraries.
     * @throws ManagementException if error or exception occurs.
     */
    Set<SharedLibrary> findSharedLibraries(String componentName) throws ManagementException;
    
    SharedLibrary getSharedLibrary(String sharedLibraryName) throws ManagementException;
    
    ComponentDescriptor getDescriptor(String sharedLibraryName) throws ManagementException;
    
    String getDescriptorAsXml(String sharedLibraryName) throws ManagementException;
    
    String install(String slZipURL) throws ManagementException;
    
    void uninstall(String sharedLibraryName) throws ManagementException;
}
